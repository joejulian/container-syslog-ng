FROM registry.gitlab.com/joejulian/docker-arch:latest

RUN pacman -Syu --noconfirm grep syslog-ng && pacman -Scc --noconfirm

CMD mkdir -p /config

CMD syslog-ng --foreground --cfgfile /config/syslog-ng.conf
