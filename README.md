# syslog-ng container

## Usage

A sample pod running postfix with syslog-ng
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: postfix-deployment
  labels:
    app: postfix
spec:
  replicas: 1
  selector: 
    matchLabels
      app: postfix
  template:
    metadata:
      labels:
        app: postfix
    spec:
      containers:
        - name: postfix
          image: registry.gitlab.com/joejulian/container-postfix:latest
          imagePullPolicy: Always
          volumeMounts:
            - name: postfix_config
              mountPath: /config
        - name: syslog-ng
          image: registry.gitlab.com/joejulian/container-syslog-ng:latest
          volumeMounts:
            - name: syslog_ng_config
              mountPath: /config
      volumes:
        - name: postfix_config
          configMap:
            name: postfix
        - name: syslog_ng_config
          configMap:
            name: syslog-ng
```
